'use strict';

const Task = require('./task');

module.exports = class Compiler extends Task {
    constructor() {
        super();

        this.interface = {};
    }

    compile(code) {
        if(typeof code !== 'string') {
            throw TypeError();
        }

        throw { name: 'NotImplementedError' };
    }
}
