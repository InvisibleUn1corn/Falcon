'use strict';
const Storage = require('./helpers/storage.js').Storage;
const ipcMain = require('electron').ipcMain;

class Reciever {
    /**
     * @param {String} name
     */
    constructor(name) {
        this._name = name;
        ipcMain.on(this._name, this._callback.bind(this));
    }

    _callback(event, arg) {
        event.sender.send(`{error: true, got: ${arg}}`);   
    }
}

class ProjectReciever extends Reciever {
    constructor(name) {
        super(name);
    }

    _callback(event, arg) {
        let projects = new Storage('projects', () => {
            if(arg == '') {
                projects.keys((keys) => {
                    console.log('test');
                    event.sender.send('projects-list', `{"error": false, "keys": ${JSON.stringify(keys)}}`);
                });
            } else {
                projects.get(arg, (err, value) => {
                    if(!err) {
                        event.sender.send('project-info', JSON.stringify(Object.assign({error: false}, JSON.parse(value))));
                    }
                });
            }
        });
    }
}

exports.Reciever = Reciever;
exports.ProjectReciever = ProjectReciever;
