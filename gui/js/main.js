'use strict';
const ipcRenderer = require('electron').ipcRenderer;
const fs = require('fs');
const logs = require('./js/logs.js');

let TABS = {
    OVERVIEW: 1,
    FILES: 2,
    LOGS: 3,
    SETTINGS: 4,
    TASKS: 5
};

let projects = [];
let currentProject = null;
let tree = undefined;
let tab = TABS.FILES;

function addProject(event, arg) {
    projects.push(arg);
    localStorage.setItem('projects', JSON.stringify(projects));
    updateProjects();
}

function updateProjects(event, arg) {
    $('#projects-sidebar .item').not(document.getElementById('new-project')).remove();

    let json = JSON.parse(arg);
    if(json.error === false) {
        projects = json.keys;

        for(let i=0; i<projects.length; i++) {
            let name = projects[i];
            $('#new-project').before(`<a class="item" onclick="setProject(${i})">${name} <i class="folder icon"></i></a>`);
        }

        if(currentProject === null) {
            setProject(0);
        }
    }
}

function setProject(id) {
    currentProject = projects[id];
    if (!fs.existsSync(`${currentProject}/.falcon`)){
        fs.mkdirSync(`${currentProject}/.falcon`);
    }

    if(!fs.existsSync(`${currentProject}/.falcon/logs`)) {
        fs.closeSync(fs.openSync(`${currentProject}/.falcon/logs`, 'w'));
    }

    updateFiles();
    updateLogs();
    // updateTasks();
}

function updateTree(event, arg) {
    tree = JSON.parse(arg);
    $('#files').empty();
    let DOM = '<div class="ui accordion folder" id="top-folder">';

    DOM += updateFolder(tree.reverse());
    DOM += '</div>';

    $('#files').append(DOM);
    $('.ui.accordion').accordion({exclusive: false});
}

function updateFolder(tree) {
    let rtnVal = ``;
    for(let i=0; i<tree.length; i++) {
        rtnVal += `
            <div class="title title-${i}">
                <i class="${tree[i].type} icon"></i>
                ${tree[i].name}
            </div>
            <div class="content content-${i}">`;

        if(tree[i].type == 'folder') {
            rtnVal += '<div class="ui accordion folder">';
            rtnVal += updateFolder(tree[i].children.reverse());
            rtnVal += '</div>';
        }

        rtnVal += `</div>`;
    }

    return rtnVal;
}

function showLoading() {
    $('.content').empty();
    $('.content').append(`
        <div class="ui segment">
            <div class="ui active inverted dimmer">
                <div class="ui medium text loader">Loading</div>
            </div>
            <p></p>
        </div>
    `);
}


function updateLogs() {
    tab = TABS.LOGS;

    logs.update(function(logsData) {
        let DOM = `<div class="ui relaxed divided list" id="logs-list">`;

        for(let i=0; i<logsData.length; i++) {
            DOM += logs.getDOM(logsData[i]);
        }
        
        DOM += `</div>`;
        $('#logs').html(DOM);
    });
}

function updateFiles() {
    tab = TABS.FILES;
    ipcRenderer.send('project-tree', currentProject);
}

function showOverview() {
    $('#header-nav .active').removeClass('active');
    $('#overview').addClass('active');
    tab = TABS.OVERVIEW;
    $('.content').empty();
    // TODO: Create overview page
}

function updateTasks() {
    tab = TABS.TASKS;
    $('#tasks').append(`
        <div class="ui grid">
  <div class="eight wide column" style="border-right: 1px solid #ddd">
                         <div class="ui items">
  <div class="item">
    <div class="ui tiny image">
      <img src="/images/wireframe/image.png">
    </div>
    <div class="middle aligned content">
      <a class="header">12 Years a Slave</a>
    </div>
  </div>
  <div class="item">
    <div class="ui tiny image">
      <img src="/images/wireframe/image.png">
    </div>
    <div class="middle aligned content">
      <a class="header">My Neighbor Totoro</a>
    </div>
  </div>
  <div class="item">
    <div class="ui tiny image">
      <img src="/images/wireframe/image.png">
    </div>
    <div class="middle aligned content">
      <a class="header">Watchmen</a>
    </div>
  </div>
</div></div>
  <div class="eight wide column">B</div>
</div>`);
}

ipcRenderer.on('add-project-reply', addProject);
ipcRenderer.on('projects-list', updateProjects);
ipcRenderer.on('project-tree-reply', updateTree);

document.querySelector('#new-project').addEventListener('click', function() {
    ipcRenderer.send('add-project', 'add-project');
});

// Get list of projects
ipcRenderer.send('projects', '');

$('.menu .item[data-tab]').tab();
