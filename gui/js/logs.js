'use strict';
let logs = undefined;

function cleanArray(actual) {
    var newArray = new Array();
    for (var i = 0; i < actual.length; i++) {
        if (actual[i]) {
            newArray.push(actual[i]);
        }
    }
    return newArray;
}

/**
 * return the color of the log type
 *
 * @param {String} type - the type of the log
 * @returns {String}
 */
function color(type) {
    if(type == 'remove circle') {
        return '#db2828';
    } else if(type == 'warning sign') {
        return '#fbbd08';
    } else {
        return '#2185d0';
    }
}

function add(data) {
    $('#logs-list').append(getDOM(data));
}

function write(data) {
    let line = `${data.type},${data.time},${data.message}\n`;
    fs.appendFile(`${currentProject}/.falcon/logs`, line, function (err) {
        if(err) {
            let time = Date.now() / 1000 | 0;
            fs.appendFile(`${currentProject}/.falcon/logs`, `remove circle,${time},Error while repoting an error`, function (err) {
                if(err) {
                    console.error('Error while reporting error');
                } else {
                    add({
                        type: 'remove circle',
                        time: time,
                        message: 'Error while repoting an error'
                    });
                }
            });
        } else {
            add(data);
        }
    });
}

function update(callback) {
    fs.readFile(`${currentProject}/.falcon/logs`, 'utf8', (err, data) => {
        if (err) throw err;

        logs = cleanArray(data.split('\n')).map(val => {
            let logdata = val.split(',');
            if(logdata.length >= 3) {
                return {
                    type: logdata[0],
                    time: logdata[1],
                    message: logdata[2]
                };
            }
        });

        callback(logs);
    });
}

function getDOM(data) {
    return `
        <div class="item">
            <i style="color: ${color(data.type)}; width: 27px;" class="large ${data.type} middle aligned icon"></i>
            <div class="content">
                <span class="header">${data.message}</span>
                <div class="description">${moment(Number(data.time)).fromNow()}</div>
            </div>
        </div>
    `;
}

exports.list = logs;
exports.color = color;
exports.add = add;
exports.write = write;
exports.update = update;
exports.getDOM = getDOM;
