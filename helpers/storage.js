'use strict';
const sqlite3 = require('sqlite3').verbose();
const electron = require('electron');

let dbSerialize = false;
let db = null;

class Storage {
    /**
     * @param {String} name - the name to save and read from
     * @param {Function} callback - called when db is init
     */
    constructor(name, callback) {
        this._name = name;

        if(db !== null) {
            if(dbSerialize === true) {
                this._createTable(callback);
            } else {
                this._db.serialize(this._createTable.bind(this, callback));
            }
        } else {
            db = new sqlite3.Database(electron.app.getPath('appData') + '/falcon/data.db');
            db.serialize(this._createTable.bind(this, callback));
        }
    }

    /**
     * @param {Function} callback
     * @returns {Undefined}
     */
    _createTable(callback) {
        db.each(`SELECT count(name) FROM sqlite_master WHERE type='table' AND name='${this._name}'`, (err, row) => {
            if(err) throw err;

            if(row['count(name)'] === 0) {
                db.run(`CREATE TABLE ${this._name} (key CHAR(60) PRIMARY KEY, value TEXT)`);
            }

            dbSerialize = true;
            callback();
        });
    }

    /**
     * return a value from the database by key
     * @param {String} key
     * @param {Function} callback
     */
    get(key, callback) {
        db.each(`SELECT value FROM ${this._name} WHERE key='${key}'`, (err, row) => {
            if(row !== undefined) {
                callback(err, row.value);
            } else {
                callback(err, undefined);
            }
        });
    }

    /**
     * Update/Insert a value to the database
     * @param {String} key
     * @param {String} value
     * @param {Function} callback
     */
    set(key, value, callback = () => {}) {
        db.each(`SELECT count(key) FROM ${this._name} WHERE key='${key}'`, (err, row) => {
            if(err) throw err;

            if(row['count(key)'] === 0) {
                db.run(`INSERT INTO ${this._name} VALUES('${key}', '${value}')`);
            } else {
                db.run(`UPDATE ${this._name} SET value='${value}' WHERE key='${key}'`);
            }

            callback();
        });
    }

    /**
     * return a list of keys
     * @param {Function} callback
     */
    keys(callback) {
        let keys = [];
        db.each(`SELECT key FROM ${this._name}`, (err, row) => {
            if(err) throw err;

            keys.push(row.key);
        }, () => {
            callback(keys);
        });
    }
}

class JSONStorageDecorator extends Storage {
    /**
     * @param {String} name - the name to save and read from
     * @param {Function} callback - called when db is init
     */
    constructor(name, callback) {
        super(name, callback);
    }

    /**
     * return a value from the database by key
     * @param {String} key
     * @param {Function} callback
     */
    get(key, callback) {
        super.get(key, (err, value) => {
            callback(err, JSON.parse(value));
        });
    }

    /**
     * Update/Insert a value to the database
     * @param {String} key
     * @param {String} value
     * @param {Function} callback
     */
    set(key, value, callback = () => {}) {
        super.set(key, JSON.stringify(value), callback);
    }
}

exports.Storage = Storage;
exports.JSONStorageDecorator = JSONStorageDecorator;
