'use strict';

const electron = require('electron');
const fs = require('fs');
const projectReciever = require('./receiver.js').ProjectReciever;

module.exports = class App {
    constructor({ height = 600, width = 800, dev = false }) {
        this._height = height;
        this._width = width;
        this._dev = dev;
        this._mainWindow = null;
        this._BASE_DIR = electron.app.getPath('appData') + '/falcon';

        this._initializeFolders();

        electron.app.on('window-all-closed', this.close.bind(this));
        electron.app.on('ready', this._ready.bind(this));
    }

    /**
     * Make sure all the needed folders exist
     * @returns {Undefined}
     */
    _initializeFolders() {
        if (!fs.existsSync(this._BASE_DIR)){
            fs.mkdirSync(this._BASE_DIR);
        }
    }

    /**
     * The program start here
     * @returns {Undefined}
     */
    _ready() {
        this._mainWindow = new electron.BrowserWindow({width: 800, height: 600});   
        this._mainWindow.loadURL('file://' + __dirname + '/gui/index.html');

        new projectReciever('projects');

        // Open dev tools if development flag is true
        if(this._dev === true) {
            this._mainWindow.webContents.openDevTools();
        }

        this._mainWindow.on('closed', function() {
            this._mainWindow = null;
        });
    }

    /**
     * This function close the program when it get signal to close
     * @returns {Undefined}
     */
    close() {
        if (process.platform != 'darwin') {
            electron.app.quit();
        }
    }
};
